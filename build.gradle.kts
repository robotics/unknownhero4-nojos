import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.10"
    id("com.github.johnrengelman.shadow") version "4.0.1"
    id("org.jlleitschuh.gradle.ktlint") version "9.4.1"
    application
}
group = ""
version = "1.0"

repositories {
    mavenCentral()
    jcenter()
    maven { url = uri("https://plugins.gradle.org/m2/") }
    flatDir {
        dirs("libs")
    }
}

configure<org.jlleitschuh.gradle.ktlint.KtlintExtension> {
    disabledRules.set(setOf("no-wildcard-imports", "indent"))
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation("com.andreapivetta.kolor:kolor:1.0.0")
    implementation("org.junit.jupiter:junit-jupiter:5.4.2")
}

tasks.withType<KotlinCompile> {
    logging.captureStandardOutput(LogLevel.INFO)
    kotlinOptions.jvmTarget = "1.8"
}

tasks.withType<Jar> {
    manifest {
        attributes["Main-Class"] = "MainKt"
    }
    configurations["compileClasspath"].forEach { file: File ->
        from(zipTree(file.absoluteFile))
    }
}

task<Exec>("deployToBrick") {
    dependsOn("jar")
    commandLine("upload.bat")
}

task<Exec>("deployToBrick_Linux") {
    dependsOn("jar")
    commandLine("./upload")
}

task<Exec>("deployToBrick_Linux_WIFI") {
    dependsOn("jar")
    commandLine("./upload_wifi")
}

task<Exec>("updateIni") {
    commandLine("updateIni.bat")
}

task<Exec>("updateIni_Linux") {
    commandLine("./updateIni")
}

application {
    mainClassName = "MainKt"
}

tasks.test {
    useJUnitPlatform()
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
