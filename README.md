# UnknownHero4

This project was written as part of an university course (Practical introduction to computer vision)

The goal was to build an robot with the Lego EV3 set and program it to solve a **unknown** maze and drive through it while looking for obstacles. There are two types of obstacles. Blue blocks aren't moveable and red blocks are movable if there is a free space behind it.

## Informations

- The robot runs leJos with Java 8 as the JRE
- The project is written in Kotlin but there is also an Python implementation (Kotlin works much better)
- Gradle is used as the build tool
- Mindsensor lineleader sensor to follow the lines from the maze
- Ultrasonic and color sensor to detect blocks and their color
