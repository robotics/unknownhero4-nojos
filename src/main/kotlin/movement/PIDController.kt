package movement

import config.Config.D
import config.Config.I
import config.Config.P
import kotlin.math.pow

/**
 * P i d controller
 *
 * @constructor Create empty P i d controller
 */
class PIDController {
    private var previousError = 0.0
    private var integral = 0.0
    private var time: Long = System.currentTimeMillis()

    /**
     * Get angle
     *
     * @param sensorValues
     * @return
     */
    fun getAngle(sensorValues: FloatArray): Double {
        // delta time
        val dt = (System.currentTimeMillis() - time) / 1000.0
        // reset time
        time = System.currentTimeMillis()

        // Use Sensor data to determine the position of the line on the sensor (between 0 and 8)
        val position = getPositionOfLineOnSensor(sensorValues)

        // P
        val error = position.pow(2) * if (position < 0) -1 else 1
        // I
        integral = integral * 0.7 + error * dt
        // D
        val derivation = if (dt != 0.0) (error - previousError) / dt else 0.0

        previousError = error

        return P * error + I * integral + D * derivation
    }

    private fun getPositionOfLineOnSensor(sensorValues: FloatArray): Double {
        val sum = sensorValues.sum()
        val weightedValues = sensorValues.mapIndexed { index, value ->
            (index + 1) * value
        }.sum()
        return weightedValues / sum - 4.5
    }

    /**
     * Reset
     *
     */
    fun reset() {
        time = System.currentTimeMillis()
        previousError = 0.0
        integral = 0.0
    }
}
