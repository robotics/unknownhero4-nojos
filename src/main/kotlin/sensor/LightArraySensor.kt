package sensor

import config.Config
import lejos.hardware.Brick
import lejos.hardware.port.SensorPort
import lejos.hardware.sensor.MindsensorsLineLeader
import lejos.hardware.sensor.SensorMode
import model.Status
import java.util.concurrent.ExecutorService

/**
 * Light array sensor
 *
 * @constructor Create empty Light array sensor
 */
class LightArraySensor : MindsensorsLineLeader(SensorPort.S1) {
    private lateinit var asyncQueue: ExecutorService
    private var paused = true
    var maxSensorValues = FloatArray(8)

    /**
     * Input values
     *
     * Every access to this value will update it to the current sensor data
     */
    val inputValues: FloatArray
        get() {
            val sample = FloatArray(8)
            fetchSample(sample, 0)
            return sample.map { 100 - it }.toFloatArray()
        }

    override fun init() {
        setModes(
            arrayOf<SensorMode>(
                FloatRedMode()
            )
        )
        setCurrentMode(0)
    }

    /**
     * Calibrate
     *
     * This method will ask the user to either go through a calibration routine or continue with the saved values
     *
     * @param brick the brick the LightArraySensor is connected to.
     */
    fun calibrate(brick: Brick) {
        val leftKey = brick.getKey("Left")
        val rightKey = brick.getKey("Right")
        val lcd = brick.textLCD
        lcd.drawString("Press Left:", 1, 1)
        lcd.drawString("Calibrate", 1, 2)
        lcd.drawString("Press Right:", 1, 3)
        lcd.drawString("Start", 1, 4)
        while (true) {
            if (leftKey.isDown) {
                val enterKey = brick.getKey("Enter")

                lcd.clear()
                lcd.drawString("Press Enter:", 1, 1)
                lcd.drawString("Black", 1, 2)
                enterKey.waitForPressAndRelease()
                calibrateBlack()

                lcd.clear()
                lcd.drawString("Press Enter:", 1, 1)
                lcd.drawString("White", 1, 2)
                enterKey.waitForPressAndRelease()
                calibrateWhite()

                lcd.clear()
                break
            } else if (rightKey.isDown) {
                lcd.clear()
                break
            }
        }
    }

    private inner class FloatRedMode : SensorMode {
        override fun getName(): String {
            return "FloatRedMode"
        }

        override fun sampleSize(): Int {
            return 8
        }

        override fun fetchSample(sample: FloatArray, offset: Int) {
            val buffer = ByteArray(sampleSize())
            this@LightArraySensor.getData(73, buffer, sampleSize())
            for (i in 0 until sampleSize()) {
                sample[offset + i] = buffer[i].toFloat()
            }
        }
    }

    companion object {
        fun verify(lightArrayValues: FloatArray): Status {
            val sum = lightArrayValues.sum()
            return when {
                sum > Config.JUNCTION_FOUND_THRESHOLD &&
                        (lightArrayValues[0] > Config.JUNCTION_FOUND_CORNER_THRESHOLD ||
                                lightArrayValues[7] > Config.JUNCTION_FOUND_CORNER_THRESHOLD) -> Status.JUNCTION_ENCOUNTERED
                sum < Config.LINE_LOST_THRESHOLD -> Status.LINE_LOST
                else -> Status.SUCCESS
            }
        }
    }
}
