package sensor

import exceptions.UnexpectedSensorValueException
import lejos.hardware.port.SensorPort
import lejos.hardware.sensor.EV3UltrasonicSensor

/**
 * Ultrasonic sensor
 *
 * @constructor Create empty Ultrasonic sensor
 */
class UltrasonicSensor {
    private val sensor = EV3UltrasonicSensor(SensorPort.S2)

    /**
     * Get distance
     *
     * @return
     */
    fun getDistance(): Float {
        val sample = FloatArray(1)
        sensor.distanceMode.fetchSample(sample, 0)

        if (sample[0] < 0)
            throw UnexpectedSensorValueException("Ultrasonic has invalid value: " + sample[0])

        return sample[0]
    }
}
