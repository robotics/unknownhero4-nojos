package sensor

import lejos.hardware.port.SensorPort
import lejos.hardware.sensor.EV3ColorSensor

/**
 * Color sensor
 *
 * @constructor Create empty Color sensor
 */
class ColorSensor {
    private val sensor = EV3ColorSensor(SensorPort.S3)

    /**
     * Get color i d
     *
     * @return
     */
    fun getColorID(): Float {
        val sample = FloatArray(1)
        sensor.colorIDMode.fetchSample(sample, 0)
        return sample[0]
    }

    /**
     * Get r g b
     *
     * @return
     */
    fun getRGB(): Triple<Float, Float, Float> {
        val sample = FloatArray(3)
        sensor.rgbMode.fetchSample(sample, 0)
        return Triple(sample[0], sample[1], sample[2])
    }
}
