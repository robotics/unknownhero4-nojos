package config

object Config {
    private val iniFile = IniFile("no-map-config.ini")
    val P = (iniFile.getOrDefault("P", 2.5) as String).toDouble()
    val I = (iniFile.getOrDefault("I", 0.1) as String).toDouble()
    val D = (iniFile.getOrDefault("D", 0.5) as String).toDouble()

    val MAP_PATH = iniFile.getOrDefault("MAP_PATH", "/home/lejos/programs/asciimap") as String

    val TILE_LENGTH = (iniFile.getOrDefault("TILE_LENGTH", 640.0) as String).toDouble() // Degrees the wheel turns

    val SPEED_PERCENT = (iniFile.getOrDefault("SPEED_PERCENT", 0.4) as String).toDouble()
    val SPEED_ANGULAR_PERCENT = (iniFile.getOrDefault("SPEED_ANGULAR_PERCENT", 0.5) as String).toDouble()

    val BLOCK_DETECTION_DISTANCE =
        (iniFile.getOrDefault("BLOCK_DETECTION_DISTANCE", 0.075) as String).toDouble() // Meter
    val BLOCK_PUSH_SPEED_PERCENT = (iniFile.getOrDefault("BLOCK_PUSH_SPEED_PERCENT", 0.4) as String).toDouble()
    val BLOCK_PUSH_SPEED_ANGULAR_PERCENT = (iniFile.getOrDefault("BLOCK_PUSH_SPEED_PERCENT", 0.5) as String).toDouble()
    val BLOCK_PUSH_DISTANCE =
        (iniFile.getOrDefault("BLOCK_PUSH_DISTANCE", 230.0) as String).toDouble() // Degrees the wheel turns

    val LINE_LOST_THRESHOLD = (iniFile.getOrDefault("LINE_LOST_THRESHOLD", 10.0) as String).toDouble()

    val JUNCTION_IGNORE_DISTANCE =
        (iniFile.getOrDefault("JUNCTION_IGNORE_DISTANCE", 200.0) as String).toDouble() // Degrees the wheel turns
    val JUNCTION_FOUND_THRESHOLD = (iniFile.getOrDefault("JUNCTION_FOUND_THRESHOLD", 250.0) as String).toDouble()
    val JUNCTION_FOUND_CORNER_THRESHOLD =
        (iniFile.getOrDefault("JUNCTION_FOUND_CORNER_THRESHOLD", 80.0) as String).toDouble()
    val JUNCTION_FOUND_FULL_CORNER_THRESHOLD =
        (iniFile.getOrDefault("JUNCTION_FOUND_FULL_CORNER_THRESHOLD", 700.0) as String).toDouble()

    val WHEEL_DIAMETER = (iniFile.getOrDefault("WHEEL_DIAMETER", 5.6) as String).toDouble() // Centimeter
    val WHEEL_OFFSET_LEFT = (iniFile.getOrDefault("WHEEL_OFFSET_LEFT", -5.95) as String).toDouble() // Centimeter
    val WHEEL_OFFSET_RIGHT = (iniFile.getOrDefault("WHEEL_OFFSET_RIGHT", 5.95) as String).toDouble() // Centimeter
    val WHEEL_PORT_LEFT = iniFile.getOrDefault("WHEEL_PORT_LEFT", "B") as String // Port
    val WHEEL_PORT_RIGHT = iniFile.getOrDefault("WHEEL_PORT_RIGHT", "C") as String // Port
    val DEBUG = (iniFile.getOrDefault("DEBUG", true) as String).toBoolean() // Centimeter
}
