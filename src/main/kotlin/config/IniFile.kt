package config

import java.io.*
import java.nio.charset.StandardCharsets
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Ini file
 *
 * @constructor
 *
 * @param filePath
 */
class IniFile(filePath: String) : Properties() {
    private val dest: File = File(filePath)

    init {
        if (dest.exists()) {
            loadFromFile(dest)
        }
    }

    /**
     * Add default
     *
     * @param key
     * @param value
     */
    fun addDefault(key: String?, value: String?) {
        if (!containsKey(key)) {
            setProperty(key, value)
            saveToFile()
        }
    }

    override fun getOrDefault(key: Any?, defaultValue: Any?): Any {
        if (!super.contains(key)) {
            addDefault(key.toString(), defaultValue.toString())
        }

        return super.getOrDefault(key, defaultValue)
    }

    /**
     * Save to file
     *
     * @return
     */
    fun saveToFile(): Boolean {
        val success = AtomicBoolean(false)
        try {
            OutputStreamWriter(FileOutputStream(dest)).use { writer ->
                if (!dest.exists()) success.set(dest.createNewFile())
                store(writer, "")
            }
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
        return success.get()
    }

    /**
     * Load from file
     *
     * @param file
     */
    private fun loadFromFile(file: File) {
        clear()
        try {
            InputStreamReader(FileInputStream(file), StandardCharsets.UTF_8).use { reader -> load(reader) }
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
    }
}
