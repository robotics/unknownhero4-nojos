import config.Config
import config.Config.MAP_PATH
import config.Config.TILE_LENGTH
import model.Direction
import model.Move
import model.Move.*
import model.Node
import model.Status
import movement.RobotController
import navigation.Pathfinder

/**
 * Controller
 *
 * @constructor Create Controller and initialize Pathfinder and RobotController
 */
class Controller {
    private val pathfinder = Pathfinder(MAP_PATH)
    private val robot = RobotController()
    private val timeTracker = TimeTracker()

    init {
        robot.calibrate()
        timeTracker.startTimings()
        recalculatePath()
        timeTracker.stopTimings()

    }

    private lateinit var movesIterator: Iterator<Move>
    private lateinit var positionsIterator: Iterator<Node>
    private var lastPosition: Node? = null

    /**
     * Start
     *
     * Starts the robot.
     * This method will read the data from the pathfinder and issue commands to the robot controller accordingly
     */
    fun start() {
        while (movesIterator.hasNext()) {
            val move = movesIterator.next()

            robot.printToScreen(move.toString(), clear = true)

            if (Config.DEBUG && move !in listOf(TURN_LEFT, TURN_RIGHT, TURN_AROUND))
                robot.waitForButtonPress()

            timeTracker.startTimings()

            val status: Status = when (move) {
                TURN_RIGHT -> {
                    // Turning right gives no feedback, so we just return success status after updating pathfinder.
                    pathfinder.turnRight()
                    robot.turnRight()
                    Status.SUCCESS
                }
                TURN_AROUND -> {
                    // Turning right gives no feedback, so we just return success status after updating pathfinder.
                    pathfinder.turnLeft()
                    pathfinder.turnLeft()
                    robot.turnAround()
                    Status.SUCCESS
                }
                TURN_LEFT -> {
                    // Turning left gives no feedback, so we just return success status after updating pathfinder.
                    pathfinder.turnLeft()
                    robot.turnLeft()
                    Status.SUCCESS
                }
                NEXT_JUNCTION -> {
                    // When driving to the next junction the robot only stops when a block or a junction is encountered.
                    lastPosition = pathfinder.robotPosition
                    pathfinder.robotPosition = positionsIterator.next()

                    val status = robot.driveToPosition(
                        forward = true,
                        detectBlock = true,
                        ignoreStartingJunction = true,
                        ignoreLineLost = false
                    )
                    if (status == Status.SUCCESS &&
                        !pathfinder.robotPosition.visited &&
                        pathfinder.robotPosition != pathfinder.mazeReader.goal
                    ) {
                        pathfinder.robotPosition.visited = true
                        pathfinder.nodeDiscovered(robot.calculateJunctionType())
                        pathfinder.turnRight()
                        recalculatePath()
                    }
                    status
                }
                NEXT_POSITION -> {
                    // When driving to the next position the robot keeps an eye on the driven distance,
                    // to stop even when no junction is encountered.
                    lastPosition = pathfinder.robotPosition
                    pathfinder.robotPosition = positionsIterator.next()
                    robot.printToScreen("Driving to: ${pathfinder.robotPosition}", true)
                    val status = robot.driveToPosition(
                        forward = true,
                        detectBlock = true,
                        ignoreStartingJunction = true,
                        ignoreLineLost = false,
                        distance = TILE_LENGTH
                    )
                    if (status == Status.SUCCESS &&
                        !pathfinder.robotPosition.visited &&
                        pathfinder.robotPosition != pathfinder.mazeReader.goal
                    ) {
                        pathfinder.robotPosition.visited = true
                        pathfinder.nodeDiscovered(robot.calculateJunctionType())
                        pathfinder.turnRight()
                        recalculatePath()
                    }
                    status
                }
                PUSH_BLOCK -> {
                    lastPosition = pathfinder.robotPosition
                    pathfinder.robotPosition = positionsIterator.next()
                    robot.pushBlock()
                    pathfinder.robotPosition.visited = true
                    pathfinder.nodeDiscovered(robot.calculateJunctionType().also { println(it) })
                    when (pathfinder.orientation) {
                        Direction.UP -> pathfinder.robotPosition.up?.blueBlock = true
                        Direction.RIGHT -> pathfinder.robotPosition.right?.blueBlock = true
                        Direction.DOWN -> pathfinder.robotPosition.down?.blueBlock = true
                        Direction.LEFT -> pathfinder.robotPosition.left?.blueBlock = true
                    }
                    pathfinder.turnRight()
                    recalculatePath()

                    Status.SUCCESS
                }
            }

            if (status == Status.BLUE_BLOCK_ENCOUNTERED) {
                // If a blue block has been encountered we want to update the map,
                // drive back to the last node and recalculate our path
                pathfinder.robotPosition.blueBlock = true

                robot.driveToPosition(
                    forward = false,
                    detectBlock = false,
                    ignoreStartingJunction = false,
                    ignoreLineLost = false
                )
                robot.driveToPosition(
                    forward = true,
                    detectBlock = false,
                    ignoreStartingJunction = false,
                    ignoreLineLost = true
                )

                if (lastPosition == null) {
                    throw Exception("There is no known Node behind the current position")
                }
                pathfinder.robotPosition = lastPosition as Node

                recalculatePath()
            } else if (status == Status.RED_BLOCK_ENCOUNTERED) {
                // If a red block has been encountered we want to update the map and either push the block and
                // come back to our current position or drive back to the last node
                val block = pathfinder.robotPosition
                block.redBlock = true

                if (lastPosition == null) {
                    throw Exception("There is no known Node behind the current position")
                }
                pathfinder.robotPosition = lastPosition as Node
                recalculatePath()


                if (pathfinder.nodePath[1] != block) {
                    robot.driveToPosition(
                        forward = false,
                        detectBlock = false,
                        ignoreStartingJunction = false,
                        ignoreLineLost = false
                    )
                    robot.driveToPosition(
                        forward = true,
                        detectBlock = false,
                        ignoreStartingJunction = false,
                        ignoreLineLost = true
                    )
                }
            }
            timeTracker.stopTimings()
        }
        robot.stop()
        robot.printToScreen("Finished\n${timeTracker.totalTime} ms", clear = true)
        robot.waitForButtonPress()
    }

    /**
     * Recalculate Moves
     *
     * Tells the Pathfinder to recalculate the optimal Path.
     * Updates the Iterators accordingly
     */
    private fun recalculatePath() {
        pathfinder.aStar()
        movesIterator = pathfinder.getMovePath().iterator()
        positionsIterator = pathfinder.nodePath.iterator()

        pathfinder.robotPosition = positionsIterator.next()
        lastPosition = null
    }
}
