package model

/**
 * Node
 *
 * @property x component of the node position
 * @property y component of the node position
 * @property up neighboring node
 * @property down neighboring node
 * @property left neighboring node
 * @property right neighboring node
 * @property g a* property: cost of path from start to node
 * @property f a* property: cost of path from start to node with predicted future cost
 * @property parent a* property: parent of node in path from start to goal,
 * used to reconstruct the optimal path after goal has been found
 * @constructor Create empty Node
 */
data class Node(
    val x: Int,
    val y: Int,
    var up: Node? = null,
    var down: Node? = null,
    var left: Node? = null,
    var right: Node? = null,
    var g: Double = 0.0,
    var f: Double = 0.0,
    var parent: Node? = null,
    var blueBlock: Boolean = false,
    var redBlock: Boolean = false,
    var visited: Boolean = false
) {

    override fun hashCode(): Int {
        return Pair(x, y).hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Node

        if (Pair(x, y) != Pair(other.x, other.y)) return false

        return true
    }

    override fun toString(): String {
        return "Node(position=${Pair(x, y)}"
    }

    val isStraight: Boolean
        get() = (up == null && down == null && left != null && right != null) ||
                (left == null && right == null && up != null && down != null)

    val isDeadEnd: Boolean
        get() = listOfNotNull(up, down, left, right).size == 1
}
