package model

enum class Move {
    TURN_RIGHT,
    TURN_LEFT,
    TURN_AROUND,
    NEXT_POSITION,
    NEXT_JUNCTION,
    PUSH_BLOCK
}
