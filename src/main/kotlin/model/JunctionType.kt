package model

enum class JunctionType {
    T_JUNCTION,
    T_LEFT_JUNCTION,
    T_RIGHT_JUNCTION,
    X_JUNCTION,
    LEFT_JUNCTION,
    RIGHT_JUNCTION,
    STRAIGHT,
    UNKNOWN_JUNCTION
}
