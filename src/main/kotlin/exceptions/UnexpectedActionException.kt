package exceptions

class UnexpectedActionException(override val message: String? = null) : Exception(message)
