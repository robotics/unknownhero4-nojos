package exceptions

class UnexpectedStatusException(override val message: String? = null) : Exception(message)
