package exceptions

class UnexpectedSensorValueException(override val message: String? = null) : Exception(message)
