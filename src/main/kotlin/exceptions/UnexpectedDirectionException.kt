package exceptions

class UnexpectedDirectionException(override val message: String? = null) : Exception(message)
