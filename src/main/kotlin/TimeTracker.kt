import lejos.hardware.Sound

class TimeTracker {
    var totalTime = 0L
    private var currentTimestamp = 0L

    fun startTimings() {
        Sound.setVolume(100)
        Sound.beepSequenceUp()
        currentTimestamp = System.currentTimeMillis()
    }

    fun stopTimings() {
        if (currentTimestamp == 0L)
            return
        Sound.beepSequence()
        totalTime += System.currentTimeMillis() - currentTimestamp
        currentTimestamp = 0L
    }
}
