package navigation

import exceptions.UnexpectedDirectionException
import model.*
import model.Direction.Companion.derivative
import java.rmi.UnexpectedException
import java.util.*
import kotlin.math.pow
import kotlin.system.measureNanoTime

/**
 * Main
 *
 */
fun main() {
    val pathfinder = Pathfinder("src/main/resources/coordinates")

    println(measureNanoTime { pathfinder.aStar() })
    pathfinder.nodePath.forEach { print("$it ") }
    println(pathfinder.getDirectionPath())
    println(pathfinder.getMovePath())
    pathfinder.robotPosition = pathfinder.mazeReader.start.up!!
    pathfinder.nodeDiscovered(JunctionType.T_JUNCTION)

    pathfinder.aStar()
    pathfinder.nodePath.forEach { print("$it ") }
    println(pathfinder.getDirectionPath())
    println(pathfinder.getMovePath())
    pathfinder.orientation = Direction.RIGHT
    pathfinder.robotPosition = pathfinder.mazeReader.start.up!!.right!!
    pathfinder.nodeDiscovered(JunctionType.T_LEFT_JUNCTION)
    pathfinder.aStar()
    pathfinder.nodePath.forEach { print("$it ") }
    println(pathfinder.getDirectionPath())
    println(pathfinder.getMovePath())
    pathfinder.robotPosition = pathfinder.mazeReader.start.up!!.right!!.right!!
    pathfinder.nodeDiscovered(JunctionType.RIGHT_JUNCTION)
    pathfinder.aStar()
    // pathfinder.printPath()
    pathfinder.nodePath.forEach { print("$it ") }

    println()
    println(pathfinder.getDirectionPath())
    println(pathfinder.getMovePath())
}

/**
 * Pathfinder
 *
 * This class gets a node network from the [CoordinatesReader] and calculates an optimal path through the maze.
 * Also it handles the robot position and orientation.
 *
 * @constructor
 *
 * @param path to the file with the maze
 */
class Pathfinder(path: String) {
    // CoordinatesReader for the given maze
    val mazeReader: CoordinatesReader = CoordinatesReader(path)

    // Orientation of the robot
    lateinit var orientation: Direction

    // Orientation needed at the beginning of the A-Star path
    private lateinit var desiredOrientation: Direction

    // Position of the robot
    var robotPosition: Node = mazeReader.start

    // Path with nodes which should be taken to reach the goal
    var nodePath = mutableListOf<Node>()

    /**
     * Turn robot orientation to the right
     *
     */
    fun turnRight() {
        orientation = orientation.turnRight()
    }

    /**
     * Turn robot orientation to the left
     *
     */
    fun turnLeft() {
        orientation = orientation.turnLeft()
    }

    /**
     * Gets direction path from the node path
     *
     * @return direction path
     */
    fun getDirectionPath(): MutableList<Direction> {
        val directions = mutableListOf<Direction>()
        var previousX = nodePath[0].x
        var previousY = nodePath[0].y
        // Looks for the direction the robot has to drive to get to the next node and adds it to the direction path for the whole node path
        nodePath.forEach {
            if (it != nodePath.first()) {
                val direction = actionToDirection(
                    Pair(it.x - previousX, it.y - previousY)
                )
                directions.add(direction)
            }
            previousX = it.x
            previousY = it.y
        }
        return directions
    }

    /**
     * Get move path from direction and node path
     *
     * @return move path
     */
    fun getMovePath(): MutableList<Move> {
        // First move is always drive to next position
        val moves = mutableListOf(Move.NEXT_POSITION)
        val directionPath = getDirectionPath()

        // Adds all moves needed for each two directions in the direction path
        directionPath.forEachIndexed { i, _ ->
            if (i != directionPath.size - 1) {
                moves.addAll(derivative(directionPath[i], directionPath[i + 1]))
            }
        }
        val nodeIterator = nodePath.iterator()
        nodeIterator.next()

        // Checks if the next node is not straight -> it is a junction -> change NEXT_POSITION to NEXT_JUNCTION
        moves.forEachIndexed { index, move ->
            if (move == Move.NEXT_POSITION) {
                val node = nodeIterator.next()
                if (!(node.isStraight || node.isDeadEnd)) {
                    moves[index] = Move.NEXT_JUNCTION
                }
                if (node.redBlock) {
                    moves[index] = Move.PUSH_BLOCK
                }
            }
        }

        // Adds TURN_LEFT / TURN_RIGHT to get the desiredOrientation needed for A-Star
        if (orientation != desiredOrientation) {
            if (orientation.turnLeft() == desiredOrientation) {
                moves.add(0, Move.TURN_LEFT)
            }
            if (orientation.turnLeft().turnLeft() == desiredOrientation) {
                moves.add(0, Move.TURN_AROUND)
            }
            if (orientation.turnRight() == desiredOrientation) {
                moves.add(0, Move.TURN_RIGHT)
            }
        }

        return moves
    }

    /**
     * A star finds a optimal path through the maze.
     *
     * @return optimal node path through the given maze
     */
    fun aStar(): Double {
        // Resets parents for all nodes
        mazeReader.nodes.forEach { node ->
            node.parent = null
        }

        nodePath = mutableListOf()
        val start = robotPosition
        val goal = mazeReader.goal
        var bestNode = start
        var bestF = Double.MAX_VALUE

        // List with touched nodes but not already visited
        val openList: PriorityQueue<Node> = PriorityQueue(Comparator.comparingDouble { it.f })
        openList.add(start)

        // List with visited nodes
        val closedList = mutableListOf<Node>()

        // Loop while there are still unvisited Nodes
        while (openList.size > 0) {
            // Takes node with the smallest cost out of the open list and adds it to the closed list
            val currentNode = openList.poll()
            closedList.add(currentNode)

            // Tests if goal is found
            if (currentNode == goal) {
                // Path reconstruction from parents
                resolvePath(currentNode)
                if (goal.g == Double.MAX_VALUE) {
                    val removeIndex = nodePath.indexOf(nodePath.find { it.g == Double.MAX_VALUE })
                    if (removeIndex < 2) return Double.MAX_VALUE
                    val node1 = nodePath[removeIndex - 1]
                    val node2 = nodePath[removeIndex - 2]
                    removeConnection(node1, node2)
                    val returnValue = aStar()
                    addConnection(node1, node2)
                    return returnValue
                }
                return goal.g
            }
            // Looks for all children and adds them to a list
            val children = mutableListOf<Node>()
            currentNode.up?.let {
                children.add(it)
            }
            currentNode.down?.let {
                children.add(it)
            }
            currentNode.right?.let {
                children.add(it)
            }
            currentNode.left?.let {
                children.add(it)
            }

            // Tests if the child wasn't already visited or if it is in the open list with an lower cost -> if its not the case add to open list
            children.forEach child@{ child ->
                if (closedList.contains(child)) return@child
                // Adds 1 to the past cost -> Child is one step further

                var g = currentNode.g + 1
                if (child.down == currentNode && currentNode.down != currentNode.parent) g += 1
                if (child.up == currentNode && currentNode.up != currentNode.parent) g += 1
                if (child.left == currentNode && currentNode.left != currentNode.parent) g += 1
                if (child.right == currentNode && currentNode.right != currentNode.parent) g += 1
                if (child.blueBlock) g += Double.MAX_VALUE
                if (child.redBlock) {
                    child.up?.blueBlock = false
                    child.down?.blueBlock = false
                    child.right?.blueBlock = false
                    child.left?.blueBlock = false
                    when (currentNode) {
                        child.down -> {
                            g += calculateBlockCost(child.up)
                        }
                        child.up -> {
                            g += calculateBlockCost(child.down)
                        }
                        child.left -> {
                            g += calculateBlockCost(child.right)
                        }
                        child.right -> {
                            g += calculateBlockCost(child.left)
                        }
                    }
                }
                // Calculate distance to goal and take it as a heuristic future cost
                val h = ((child.x - goal.x).toDouble().pow(2)) + (
                        (child.y - goal.y).toDouble().pow(2))

                if (openList.any { it == child && g > it.g }) return@child
                if (g + h < bestF && (!child.visited || bestNode == mazeReader.start)) {
                    bestF = g + h
                    bestNode = child
                }
                child.g = g
                child.f = g + h

                // Set parent for child
                child.parent = currentNode

                openList.add(child)
            }
        }
        resolvePath(bestNode)
        return Double.MAX_VALUE
    }

    private fun resolvePath(node: Node) {
        var currentNode: Node? = node
        while (currentNode != null) {
            nodePath.add(currentNode)
            currentNode = currentNode.parent
        }
        nodePath.reverse()

        // Calculate needed Orientation for the path
        val firstDirection = when {
            nodePath[0].up == nodePath[1] -> Direction.UP
            nodePath[0].down == nodePath[1] -> Direction.DOWN
            nodePath[0].left == nodePath[1] -> Direction.LEFT
            nodePath[0].right == nodePath[1] -> Direction.RIGHT
            else -> throw UnexpectedDirectionException()
        }
        // If robot orientation is Unknown set it to the desired orientation
        if (!this::orientation.isInitialized) {
            orientation = firstDirection
            desiredOrientation = firstDirection
        } else {
            desiredOrientation = firstDirection
        }
    }

    private fun calculateBlockCost(node: Node?): Double {
        return if (node != null && node != mazeReader.goal) {
            node.blueBlock = true
            5.0
        } else if (node == null) {
            1000.0
        } else {
            Double.MAX_VALUE
        }
    }

    fun nodeDiscovered(junctionType: JunctionType) {
        when (junctionType) {
            JunctionType.T_JUNCTION -> {
                addNeighbour(robotPosition, orientation.turnLeft())
                addNeighbour(robotPosition, orientation.turnRight())
            }
            JunctionType.X_JUNCTION -> {
                addNeighbour(robotPosition, orientation.turnLeft())
                addNeighbour(robotPosition, orientation.turnRight())
                addNeighbour(robotPosition, orientation)
            }
            JunctionType.LEFT_JUNCTION -> addNeighbour(robotPosition, orientation.turnLeft())
            JunctionType.RIGHT_JUNCTION -> addNeighbour(robotPosition, orientation.turnRight())
            JunctionType.UNKNOWN_JUNCTION -> throw UnexpectedException("No junction type found!")
            JunctionType.T_LEFT_JUNCTION -> {
                addNeighbour(robotPosition, orientation.turnLeft())
                addNeighbour(robotPosition, orientation)
            }
            JunctionType.T_RIGHT_JUNCTION -> {
                addNeighbour(robotPosition, orientation.turnRight())
                addNeighbour(robotPosition, orientation)
            }
            JunctionType.STRAIGHT -> addNeighbour(robotPosition, orientation)
        }
    }

    private fun addNeighbour(originNode: Node, direction: Direction) {
        when (direction) {
            Direction.UP -> {
                val existingNode = mazeReader.nodes.find { it.x == originNode.x && it.y == originNode.y - 1 }
                if (existingNode != null) {
                    originNode.up = existingNode
                    existingNode.down = originNode
                } else {
                    originNode.up = Node(originNode.x, originNode.y - 1)
                    mazeReader.nodes.add(originNode.up!!)
                    originNode.up!!.down = originNode
                }
            }
            Direction.RIGHT -> {
                val existingNode = mazeReader.nodes.find { it.x == originNode.x + 1 && it.y == originNode.y }
                if (existingNode != null) {
                    originNode.right = existingNode
                    existingNode.left = originNode
                } else {
                    originNode.right = Node(originNode.x + 1, originNode.y)
                    mazeReader.nodes.add(originNode.right!!)
                    originNode.right!!.left = originNode
                }
            }
            Direction.DOWN -> {
                val existingNode = mazeReader.nodes.find { it.x == originNode.x && it.y == originNode.y + 1 }
                if (existingNode != null) {
                    originNode.down = existingNode
                    existingNode.up = originNode
                } else {
                    originNode.down = Node(originNode.x, originNode.y + 1)
                    mazeReader.nodes.add(originNode.down!!)
                    originNode.down!!.up = originNode
                }
            }
            Direction.LEFT -> {
                val existingNode = mazeReader.nodes.find { it.x == originNode.x - 1 && it.y == originNode.y }
                if (existingNode != null) {
                    originNode.left = existingNode
                    existingNode.right = originNode
                } else {
                    originNode.left = Node(originNode.x - 1, originNode.y)
                    mazeReader.nodes.add(originNode.left!!)
                    originNode.left!!.right = originNode
                }
            }
        }
    }

    companion object {

        /**
         * Action to direction
         *
         * @param vector
         */
        fun actionToDirection(vector: Pair<Int, Int>) = when (vector) {
            Pair(0, -2), Pair(0, -1) -> Direction.UP
            Pair(0, 2), Pair(0, 1) -> Direction.DOWN
            Pair(2, 0), Pair(1, 0) -> Direction.RIGHT
            Pair(-2, 0), Pair(-1, 0) -> Direction.LEFT
            else -> throw UnexpectedDirectionException()
        }

        /**
         * Remove connection
         *
         * @param node1
         * @param node2
         */
        fun removeConnection(node1: Node, node2: Node) {
            if (node1.up === node2) {
                node1.up = null
                node2.down = null
            }
            if (node1.down === node2) {
                node1.down = null
                node2.up = null
            }
            if (node1.right === node2) {
                node1.right = null
                node2.left = null
            }
            if (node1.left === node2) {
                node1.left = null
                node2.right = null
            }
        }

        /**
         * Add connection
         *
         * @param node1
         * @param node2
         */
        fun addConnection(node1: Node, node2: Node) {
            if (node1.x > node2.x && node1.y == node2.y) {
                node1.left = node2
                node2.right = node1
            }
            if (node1.x < node2.x && node1.y == node2.y) {
                node1.right = node2
                node2.left = node1
            }
            if (node1.x == node2.x && node1.y > node2.y) {
                node1.down = node2
                node2.up = node1
            }
            if (node1.x == node2.x && node1.y < node2.y) {
                node1.up = node2
                node2.down = node1
            }
        }
    }
}
