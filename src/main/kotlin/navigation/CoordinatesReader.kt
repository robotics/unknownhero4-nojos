package navigation

import model.Node
import java.io.File

/**
 * Maze reader
 *
 * This class reads an given maze file in Ascii format and parses it to a node network.
 *
 * @constructor
 *
 * @param path to the file with the maze
 */
class CoordinatesReader(path: String) {
    // Stores start node
    var start: Node

    // Stores goal node
    var goal: Node

    // Stores nodes in a 2D-Array
    val nodes: MutableList<Node> = mutableListOf()

    init {
        // Reads maze file and creates nodes on the given positions
        val startCoords = File(path).readLines()[0].split(" ")
        val goalCoords = File(path).readLines()[1].split(" ")
        start = Node(startCoords[0].toInt(), startCoords[1].toInt())
        start.up = Node(start.x, start.y - 1)
        goal = Node(goalCoords[0].toInt(), goalCoords[1].toInt())
        nodes.addAll(listOf(start, goal, start.up!!))
    }
}
